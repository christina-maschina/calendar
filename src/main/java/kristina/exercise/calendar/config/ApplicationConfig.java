package kristina.exercise.calendar.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration

public class ApplicationConfig implements WebMvcConfigurer {

   @Bean
   public ModelMapper modelMapper() {
      ModelMapper modelMapper = new ModelMapper();
      return modelMapper;
   }

   /*@Bean
   public SpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
      SpringTemplateEngine templateEngine = new SpringTemplateEngine();
      templateEngine.addDialect(new Java8TimeDialect());
      templateEngine.setTemplateResolver(templateResolver);
      templateEngine.setEnableSpringELCompiler(true);
      return templateEngine;
   }*/




}