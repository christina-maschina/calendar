package kristina.exercise.calendar.service;

import kristina.exercise.calendar.transfer.EventDTO;
import java.util.List;

public interface EventService {

    void addNewEvent(EventDTO eventDTO);

    List<EventDTO> getAll();

    EventDTO getById(long id) throws IllegalArgumentException;

    EventDTO findByDateId(Long id);

}
