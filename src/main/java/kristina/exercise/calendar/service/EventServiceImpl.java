package kristina.exercise.calendar.service;

import kristina.exercise.calendar.domain.DateTime;
import kristina.exercise.calendar.domain.Event;
import kristina.exercise.calendar.domain.SortEventsByTime;
import kristina.exercise.calendar.repository.DateTimeRepository;
import kristina.exercise.calendar.repository.EventRepository;
import kristina.exercise.calendar.transfer.DateTimeDTO;
import kristina.exercise.calendar.transfer.EventDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepo;
    @Autowired
    private DateTimeRepository dateTimeRepo;
    @Autowired
    private ModelMapper modelMapper;

    final long duration = 2 * 60 * 60 * 1000;

    private void changeTimeZone(DateTimeDTO dateDTO, String sign) {
        if (sign.equals("-")) {
            dateDTO.getStartDate().setTime(dateDTO.getStartDate().getTime() - duration);
            dateDTO.getEndDate().setTime(dateDTO.getEndDate().getTime() - duration);
        } else {
            dateDTO.getStartDate().setTime(dateDTO.getStartDate().getTime() + duration);
            dateDTO.getEndDate().setTime(dateDTO.getEndDate().getTime() + duration);
        }
    }

    @Override
    public void addNewEvent(EventDTO eventDTO) {

        Set<DateTime> dateTimeSet = new HashSet<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(eventDTO.getDateTimeDTO().getStartDateString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timestamp timestampStartDate = new Timestamp(parsedDate.getTime());

        try {
            parsedDate = dateFormat.parse(eventDTO.getDateTimeDTO().getEndDateString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timestamp timestampEndDate = new Timestamp(parsedDate.getTime());

        DateTimeDTO dateTimeDTO = new DateTimeDTO();
        dateTimeDTO.setStartDate(timestampStartDate);
        dateTimeDTO.setEndDate(timestampEndDate);

        DateTime dateTime = modelMapper.map(dateTimeDTO, DateTime.class);
        dateTimeSet.add(dateTime);

        List<Event> events = eventRepo.findAll();

        if (events.size() == 0) {
            Event event2 = modelMapper.map(eventDTO, Event.class);
            event2.setDateTimes(dateTimeSet);
            eventRepo.save(event2);

        }
        int i = 0;

        for (Event event : events) {
            i++;

            if (event.getName().equals(eventDTO.getName())) {

                DateTime dateTime2 = new DateTime();
                dateTime2.setEvent(event);
                dateTime2.setStartDate(dateTime.getStartDate());
                dateTime2.setEndDate(dateTime.getEndDate());
                dateTimeRepo.save(dateTime2);
                break;

            } else if (events.size() == i) {
                Event event2 = modelMapper.map(eventDTO, Event.class);
                event2.setDateTimes(dateTimeSet);
                eventRepo.save(event2);

            }
        }
    }

    @Override
    public List<EventDTO> getAll() {
        List<Event> events = eventRepo.findAll();

        List<EventDTO> eventDTOS = events
                .stream()
                .map(event -> {
                    Type dateTimeDtoType = new TypeToken<Set<DateTimeDTO>>() {
                    }.getType();
                    Set<DateTimeDTO> dateTimeDtoTypeSet = modelMapper.map(event.getDateTimes(), dateTimeDtoType);

                    EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
                    eventDTO.setDateTimeDTOS(dateTimeDtoTypeSet);
                    return eventDTO;
                })
                .collect(Collectors.toList());

        Collections.sort(eventDTOS, new SortEventsByTime());

        return eventDTOS;
    }

    @Override
    public EventDTO getById(long id) throws IllegalArgumentException {
        Event event = eventRepo.findById(id).orElse(null);
        if (ObjectUtils.isEmpty(event)) {
            throw new IllegalArgumentException();
        }

        EventDTO eventDTO = modelMapper.map(event, EventDTO.class);
        Set<DateTimeDTO> dateTimeDTOSet = event.getDateTimes()
                .stream()
                .map(date -> {

                    DateTimeDTO dateTimeDTO = modelMapper.map(date, DateTimeDTO.class);
                    changeTimeZone(dateTimeDTO, "+");
                    return dateTimeDTO;
                })
                .collect(Collectors.toSet());

        eventDTO.setDateTimeDTOS(dateTimeDTOSet);

        return eventDTO;
    }

    @Override
    public EventDTO findByDateId(Long id) {

        DateTime dateTime=dateTimeRepo.findById(id).orElse(null);
        Event event=eventRepo.findById(dateTime.getEvent().getId()).orElse(null);

        SimpleDateFormat dateFormat=new SimpleDateFormat("HH:mm:ss");
        String startTime= dateFormat.format(dateTime.getStartDate());
        System.out.println(startTime);
        String endTime=dateFormat.format(dateTime.getEndDate());
        System.out.println(endTime);

        EventDTO eventDTO=new EventDTO();
        eventDTO.setName(event.getName());

        //eventDTO.getDateTimeDTO().setStartDateString(startTime);
        //eventDTO.getDateTimeDTO().setEndDateString(endTime);

        return eventDTO;
    }

}
