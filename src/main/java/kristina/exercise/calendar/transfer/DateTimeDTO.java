package kristina.exercise.calendar.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class DateTimeDTO implements Serializable {

    private Long id;
    private Timestamp startDate;
    private Timestamp endDate;
    private String startDateString;
    private String endDateString;


}
