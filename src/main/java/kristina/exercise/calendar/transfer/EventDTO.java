package kristina.exercise.calendar.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;
import java.util.*;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class EventDTO implements Serializable {

   private String name;
   private DateTimeDTO dateTimeDTO;
   private Set<DateTimeDTO> dateTimeDTOS=new HashSet<>();
}
