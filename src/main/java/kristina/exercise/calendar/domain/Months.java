package kristina.exercise.calendar.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.Month;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class Months {

    private Month month;

}
