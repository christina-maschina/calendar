package kristina.exercise.calendar.domain;

import kristina.exercise.calendar.transfer.DateTimeDTO;
import kristina.exercise.calendar.transfer.EventDTO;

import java.text.SimpleDateFormat;
import java.util.Comparator;


public class SortEventsByTime implements Comparator<EventDTO> {


    @Override
    public int compare(EventDTO event1, EventDTO event2) {
        for(DateTimeDTO dateTimeDTO1 : event1.getDateTimeDTOS()){
            String date1=new SimpleDateFormat("yyyy-MM-dd").format(dateTimeDTO1.getStartDate());
            for(DateTimeDTO dateTimeDTO2 : event2.getDateTimeDTOS()){
                String date2=new SimpleDateFormat("yyyy-MM-dd").format(dateTimeDTO2.getStartDate());
                if(date1.equals(date2)){
                    return (int) (dateTimeDTO1.getStartDate().getTime() - dateTimeDTO2.getStartDate().getTime());
                }else{
                    return dateTimeDTO1.getStartDate().compareTo(dateTimeDTO2.getStartDate());
                }
            }
        }
        return 0;
    }
}
