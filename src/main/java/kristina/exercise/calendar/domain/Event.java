package kristina.exercise.calendar.domain;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "event")
@Data
public class Event implements Serializable {

    @Id
    @GeneratedValue(generator = "event_generator", strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="id_event")
    private Set<DateTime> dateTimes =new HashSet<>();
}
