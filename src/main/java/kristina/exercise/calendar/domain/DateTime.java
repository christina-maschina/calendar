package kristina.exercise.calendar.domain;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "date_time")
@Getter
@Setter
public class DateTime implements Serializable {

    @Id
    @GeneratedValue(generator = "date_time_generator", strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private long id;

    @Column(name="start_date")

    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn (name="id_event")
    private Event event;
}
