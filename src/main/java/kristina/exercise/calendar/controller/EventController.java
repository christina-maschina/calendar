package kristina.exercise.calendar.controller;

import kristina.exercise.calendar.domain.Months;
import kristina.exercise.calendar.service.EventService;
import kristina.exercise.calendar.transfer.EventDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.DateFormatSymbols;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping("/")
    public ModelAndView hello(Months months) {

        ModelAndView mv = new ModelAndView();
        mv.addObject("months", months);
        mv.setViewName("index");
        return mv;
    }

    @GetMapping("/showForm")
    public ModelAndView showForm(EventDTO eventDTO) {

        ModelAndView mv = new ModelAndView();
        mv.addObject("eventDTO", eventDTO);
        mv.setViewName("add-event");
        return mv;
    }


    //dodaje novi dogadjaj
    @PostMapping("/add")
    public String addEvent(@ModelAttribute EventDTO eventDTO) {

        eventService.addNewEvent(eventDTO);
        return "redirect:/event/";

    }

    //dohvaca sve
    @GetMapping("/get")
    public ModelAndView getAll() {

        HttpStatus responseHttpStatus = HttpStatus.OK;
        List<EventDTO> responseBody = eventService.getAll();

        final Calendar previous = Calendar.getInstance();
        previous.add(Calendar.MONTH, -1);
        final Calendar current = Calendar.getInstance();

        ModelAndView mv = new ModelAndView();

        mv.setViewName("show-events");
        mv.addObject("lastDayInPreviousMonth", previous.getActualMaximum(previous.DATE));
        mv.addObject("lastDayInCurrentMonth", current.getActualMaximum(current.DATE));
        mv.addObject("currentMonthNumber", current.get(Calendar.MONTH));
        mv.addObject("previousMonthNumber", previous.get(Calendar.MONTH));
        mv.addObject("year", previous.get(Calendar.YEAR));
        mv.addObject("currentMonthName", new DateFormatSymbols().getMonths()[current.get(Calendar.MONTH)]);

        mv.addObject("events", responseBody);

        if (ObjectUtils.isEmpty(responseBody)) {
            responseHttpStatus = HttpStatus.NO_CONTENT;
        }

        mv.setStatus(responseHttpStatus);
        return mv;
    }

    //dohvaca pojedini mjesec
    @PostMapping("/selectedMonth")
    public ModelAndView getSelectedMonth(@ModelAttribute Months month) {

        HttpStatus responseHttpStatus = HttpStatus.OK;
        List<EventDTO> responseBody = eventService.getAll();

        int monthNumber = month.getMonth().getValue();

        final Calendar previous = Calendar.getInstance();
        previous.set(Calendar.MONTH, monthNumber - 2);

        final Calendar current = Calendar.getInstance();
        current.set(Calendar.MONTH, monthNumber - 1);
        System.out.println(current.get(Calendar.MONTH));

        YearMonth yearMonth=null;
        if (monthNumber == 1) {
            yearMonth = YearMonth.of(previous.get(Calendar.YEAR), 12);
        } else {
            yearMonth = YearMonth.of(previous.get(Calendar.YEAR), monthNumber - 1);
        }
        int daysInPreviousMonth = yearMonth.lengthOfMonth();
        int year = yearMonth.getYear();

        YearMonth yearMonth2 = YearMonth.of(2020, monthNumber);
        int daysInCurrentMonth = yearMonth2.lengthOfMonth();

        ModelAndView mv = new ModelAndView();

        mv.setViewName("show-events");
        mv.addObject("lastDayInPreviousMonth", daysInPreviousMonth);
        mv.addObject("lastDayInCurrentMonth", daysInCurrentMonth);
        mv.addObject("currentMonthNumber", current.get(Calendar.MONTH));
        mv.addObject("previousMonthNumber", previous.get(Calendar.MONTH));
        mv.addObject("currentMonthName", new DateFormatSymbols().getMonths()[monthNumber - 1]);
        mv.addObject("year", year);
        mv.addObject("events", responseBody);

        mv.setStatus(responseHttpStatus);

        return mv;

    }

    //dohvaca pojedini dan
    // ------------nedovrseno---------------------------
    // ----------- nije iskoristeno---------------------
    @GetMapping("/getDay")
    public ModelAndView getSelectedDay(@RequestParam(value = "id", required = false) Long id) {

        ModelAndView mv = new ModelAndView();
        EventDTO responseBody = eventService.findByDateId(id);

        mv.setViewName("selected-event");
        mv.addObject("event", responseBody);

        return mv;
    }

}
