package kristina.exercise.calendar.repository;

import kristina.exercise.calendar.domain.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DateTimeRepository extends JpaRepository<DateTime, Long> {

}
